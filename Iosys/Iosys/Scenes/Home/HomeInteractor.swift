//
//  HomeInteractor.swift
//  Iosys
//  Template by Weslley Madeira

//  Created by Weslley Madeira on 15/01/20.
//Copyright © 2020 Weslley Madeira. All rights reserved.


import UIKit
import Alamofire

class HomeInteractor {
    
    weak var output: HomeInteractorOutput?
    var oldRequest: DataRequest?
}

extension HomeInteractor: HomeUseCase {
    
    func requestCompany(textSearch: String) {
        oldRequest?.cancel()
        oldRequest = Service.request(request: .getSearchCompany(textSearch: textSearch), expered: ResponseCompaySearch.self, completion: { (responseSuccess) in
            if let companys = responseSuccess.obj?.company {
                self.output?.companysSuccess(companys: companys)
            } else {
                self.output?.companyError(error: HttpResponseError(code: 10001, messagem: "Erro ao montar objeto"))
            }
        }, fail: { (responseError) in
            if responseError.errorCode != 0 {
                self.output?.companyError(error: responseError)
            }
        })
        
    }
    func cancelAllrequest() {
        oldRequest?.cancel()
    }
}



