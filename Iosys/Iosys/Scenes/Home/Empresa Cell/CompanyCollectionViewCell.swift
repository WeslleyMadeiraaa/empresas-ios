//
// CompanyCollectionViewCell.swift
//  Iosys
//
//  Created by Weslley Madeira on 15/01/20.
//  Copyright © 2020 Weslley Madeira. All rights reserved.
//

import UIKit
import Kingfisher

class CompanyCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelType: UILabel!
    @IBOutlet weak var labelLocalization: UILabel!
    @IBOutlet weak var imageLogo: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func bindData(company: Company) {
        self.labelTitle.text = company.enterpriseName
        self.labelType.text = company.companyType.enterpriseTypeName
        self.labelLocalization.text = company.country
        if let url = URL(string: company.photo ?? "") {
            self.imageLogo.kf.setImage(with: url)
        }
       
    }

}
