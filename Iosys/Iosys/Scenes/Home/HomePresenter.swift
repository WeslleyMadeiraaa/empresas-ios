//
//  HomePresenter.swift
//  Iosys
//  Template by Weslley Madeira

//  Created by Weslley Madeira on 15/01/20.
//  Copyright © 2020 Weslley Madeira. All rights reserved.


import UIKit


class HomePresenter {
    
    
    var companys: [Company] = [] {
        didSet {
            view.showCompanys()
            if companys.count == 0 {
                self.view.showNotFound()
            } else {
                self.view.hideNotFound()
            }
        }
    }
    weak var view: HomeView!
    var router: HomeWireFrame!
    var interactor: HomeUseCase!

}

extension HomePresenter : HomePresentation {
    
    func searchText(text: String) {
        if text.isEmpty {
            interactor.cancelAllrequest()
            companys = []
            return
        }
        interactor.requestCompany(textSearch: text)
    }
    func getCompanysSize() -> Int {
        return self.companys.count
    }
    
    func configCell(cell: CompanyCollectionViewCell, indePath: IndexPath) {
        let compay = companys[indePath.row]
        cell.bindData(company: compay)
    }
    
    func cancelSearch() {
        self.companys = []
        self.view.showCompanys()
        self.view.showTutorial()
    }
    
    func selectedCompany(indexPath: IndexPath) {
        view.hideSearchBar()
        let company = companys[indexPath.row]
        self.router.showCompanyDetail(company: company)
    }
    
    func viewDidLoad(){
        
    }
    func viewDidAppear(){
        
    }
    func viewWillAppear(_ animated: Bool){
        
    }
    func viewDidDisappear(_ animated: Bool){
        
    }
    func viewWillDisappear(_ animated: Bool){
        
    }
}

extension HomePresenter: HomeInteractorOutput {
    func companysSuccess(companys: [Company]) {
        self.companys = companys
    }
    
    func companyError(error: HttpResponseError) {
        self.view.showAlertController(title: "Error", message: error.message)
    }
    
}
