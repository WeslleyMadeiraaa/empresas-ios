//
//  HomeRouter.swift
//  IosysRouter
//  Template by Weslley Madeira

//  Created by Weslley Madeira on 15/01/20.
//  Copyright © 2020 Weslley Madeira. All rights reserved.


import UIKit


class HomeRouter {
    
    weak var view            : BaseViewController?
   
    
    static func startView() -> UINavigationController {
        
        let viewController  = HomeViewController()
        let presenter       = HomePresenter()
        let router          = HomeRouter()
        let interactor    = HomeInteractor()
        
        router.view = viewController
        
        viewController.presenter = presenter
        presenter.router = router
        presenter.view = viewController
        presenter.interactor = interactor
        
        interactor.output = presenter
        
        let navigation = UINavigationController(rootViewController: viewController)
        navigation.modalPresentationStyle = .fullScreen
        navigation.modalPresentationCapturesStatusBarAppearance = true
        return navigation
    }
}

extension HomeRouter : HomeWireFrame {
    
    func showCompanyDetail(company: Company) {
        let detail = CompanyDetailRouter.startView(company: company)
        self.view?.navigationController?.pushViewController(detail, animated: true)
    }
   
}

