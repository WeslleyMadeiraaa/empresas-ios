//
//  HomeViewController.swift
//  Iosys
//  Template by Weslley Madeira

//  Created by Weslley Madeira on 15/01/20.
//Copyright © 2020 Weslley Madeira. All rights reserved.


import UIKit


class HomeViewController: BaseViewController {
    
    var presenter: HomePresenter!
    let reusableIdentifier = "compayCell"
    
    @IBOutlet weak var collectionViewCompany: UICollectionView!
    @IBOutlet weak var labelTutorialStart: UILabel!
    @IBOutlet weak var labelNotFound: UILabel!
    
    
    var searchBar: UISearchBar?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.configNavigationBar()
        self.configCollectionView()
        self.presenter.viewDidLoad()
        setNeedsStatusBarAppearanceUpdate()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func configCollectionView() {
        let nib = UINib(nibName: "CompanyCollectionViewCell", bundle: nil)
        self.collectionViewCompany.register(nib, forCellWithReuseIdentifier: reusableIdentifier)
        self.collectionViewCompany.delegate = self
        self.collectionViewCompany.dataSource = self
    }
    
    func configNavigationBar() {
        
        navigationController?.applyIosysStyle()
        navigationItem.insertIosysLogo()
        
        self.showButtonSearch()
    }
    
    @objc func showSearch() {
        self.hiddenButtonSearch()
        searchBar = UISearchBar()
        searchBar?.showsCancelButton = true
        searchBar?.placeholder = "Procurar"
        searchBar?.delegate = self
        searchBar?.barTintColor = UIColor.whiteColor
        searchBar?.backgroundImage = UIImage()
        let textFieldInsideSearchBar = searchBar?.value(forKey: "searchField") as? UITextField
        textFieldInsideSearchBar?.backgroundColor = UIColor.whiteColor
        textFieldInsideSearchBar?.textColor = UIColor.fontBlackColor
        if let textFieldInsideSearchBar = searchBar?.value(forKey: "searchField") as? UITextField,
            let glassIconView = textFieldInsideSearchBar.leftView as? UIImageView {
            glassIconView.image = glassIconView.image?.withRenderingMode(.alwaysTemplate)
            glassIconView.tintColor = UIColor.fontBlackColor
        }
        self.navigationItem.titleView = searchBar
        searchBar?.becomeFirstResponder()
        
        
    }
    
    func setColor() {
        
    }
    
    func hiddenButtonSearch() {
        navigationItem.rightBarButtonItem = nil
    }
    
    func showButtonSearch() {
        let rightBarButtonItem = UIBarButtonItem()
        rightBarButtonItem.image = UIImage.icSearch?.withRenderingMode(.alwaysOriginal)
        rightBarButtonItem.target = self
        rightBarButtonItem.action = #selector(self.showSearch)
        navigationItem.rightBarButtonItem = rightBarButtonItem
        navigationItem.insertIosysLogo()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
}

extension HomeViewController: HomeView {
    func showNotFound() {
        self.labelNotFound.isHidden = false
        self.labelTutorialStart.isHidden = true
    }
    
    func showTutorial() {
        self.labelNotFound.isHidden = true
        self.labelTutorialStart.isHidden = false
    }
    func showCompanys() {
        self.collectionViewCompany.reloadData()
    }
    func hideNotFound() {
        self.labelNotFound.isHidden = true
        self.labelTutorialStart.isHidden = true
    }
    
    func hideSearchBar() {
        searchBar?.text = ""
        searchBar?.showsCancelButton = false
        searchBar?.endEditing(true)
        self.endEditingMode()
        navigationItem.titleView = nil
        self.showButtonSearch()
        navigationItem.insertIosysLogo()
    }
}

extension HomeViewController: UISearchBarDelegate {
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.hideSearchBar()
        self.presenter.cancelSearch()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let text = searchBar.text else {return}
        self.presenter.searchText(text: text )
    }

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.presenter.searchText(text: searchText)
    }
    
}

extension HomeViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.presenter.selectedCompany(indexPath: indexPath)
    }
    
}


extension HomeViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let companySize = presenter.getCompanysSize()
        return companySize
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reusableIdentifier, for: indexPath) as? CompanyCollectionViewCell else {return collectionView.dequeueReusableCell(withReuseIdentifier: "", for: indexPath)}
        presenter.configCell(cell: cell, indePath: indexPath)
        return cell
    }
    
    
}

extension HomeViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.width, height: 113)
    }
    
}
