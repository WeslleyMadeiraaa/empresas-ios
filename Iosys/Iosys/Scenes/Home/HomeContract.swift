//
//  HomeContract.swift
//  Iosys
//  Template by Weslley Madeira

//  Created by Weslley Madeira on 15/01/20.
//Copyright © 2020 Weslley Madeira. All rights reserved.


import UIKit


protocol HomeView: BaseView {
    
    func showCompanys()
    func showNotFound()
    func showTutorial()
    func hideNotFound()
    func hideSearchBar()
    
}

protocol HomePresentation: class {
    
    var companys: [Company] {get set}
    
    func searchText(text: String)
    func selectedCompany(indexPath: IndexPath)
    func cancelSearch()
    func getCompanysSize() -> Int
    func configCell(cell: CompanyCollectionViewCell , indePath: IndexPath)
    
    func viewDidLoad()
    func viewDidAppear()
    func viewWillAppear(_ animated: Bool)
    func viewDidDisappear(_ animated: Bool)
    func viewWillDisappear(_ animated: Bool)
    
}

protocol HomeUseCase: class {
    
    func requestCompany(textSearch: String)
    func cancelAllrequest()
}

protocol HomeInteractorOutput: class {
    
    func companysSuccess(companys: [Company])
    func companyError(error: HttpResponseError)
    
}

protocol HomeWireFrame: class {
    func showCompanyDetail(company: Company)
}


