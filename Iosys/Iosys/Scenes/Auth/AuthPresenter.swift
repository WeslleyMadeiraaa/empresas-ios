//
//  AuthPresenter.swift
//  Iosys
//  Template by Weslley Madeira

//  Created by Weslley Madeira on 15/01/20.
//  Copyright © 2020 Weslley Madeira. All rights reserved.


import UIKit


class AuthPresenter {
    
    weak var view: AuthView!
    var router: AuthWireFrame!
    var interactor: AuthUseCase!
    
    
}

extension AuthPresenter : AuthPresentation {
    
    func buttonLoginPressed(email: String, password: String) {
        view.showLoading(handle: nil)
        if email.isEmpty || password.isEmpty {
            view.hideLoading {
                self.view.showAlertController(title: "Atenção", message: "Preencher todos os campos")
            }
            return
        }

        interactor.requestLogin(email: email, password: password)
        
    }
    
    func viewDidLoad(){
        
    }
    func viewDidAppear(){
        
    }
    func viewWillAppear(_ animated: Bool){
        
    }
    func viewDidDisappear(_ animated: Bool){
        
    }
    func viewWillDisappear(_ animated: Bool){
        
    }
}

extension AuthPresenter: AuthInteractorOutput {
    func requestLoginSuccess() {
        self.router.showHome()
    }
    
    func requestLoginError(error: HttpResponseError) {
        self.view.hideLoading {
            self.view.showAlertController(title: "Erro", message: error.message)
        }
    }
    
    
}
