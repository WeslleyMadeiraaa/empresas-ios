//
//  AuthInteractor.swift
//  Iosys
//  Template by Weslley Madeira

//  Created by Weslley Madeira on 15/01/20.
//Copyright © 2020 Weslley Madeira. All rights reserved.


import UIKit

class AuthInteractor {
    
    weak var output: AuthInteractorOutput?
}

extension AuthInteractor: AuthUseCase {
    
    func requestLogin(email: String, password: String) {
        Service.request(request: .singIN(email: email, password: password), expered: ResponseAuth.self, completion: { (response) in
            
            if let uid = response.headers?["uid"] as? String ,
                let token = response.headers?["access-token"] as? String,
                let client = response.headers?["client"] as? String {
                
                let acessData = AccessData(uid: uid, client: client, accessToken: token)
                SecurityAccess.saveAccessData(acessData: acessData)
                self.output?.requestLoginSuccess()
            } else {
                self.output?.requestLoginError(error: HttpResponseError(code: 10001, messagem: "Erro ao montar dados do servidor"))
            }
            
            
        }) { (responseError) in
            self.output?.requestLoginError(error: responseError)
        }
    }
    
}


