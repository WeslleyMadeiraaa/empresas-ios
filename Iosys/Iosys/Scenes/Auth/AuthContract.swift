//
//  AuthContract.swift
//  Iosys
//  Template by Weslley Madeira

//  Created by Weslley Madeira on 15/01/20.
//Copyright © 2020 Weslley Madeira. All rights reserved.


import UIKit


protocol AuthView: BaseView {
    
}

protocol AuthPresentation: class {
    func buttonLoginPressed(email: String ,password: String)
    func viewDidLoad()
    func viewDidAppear()
    func viewWillAppear(_ animated: Bool)
    func viewDidDisappear(_ animated: Bool)
    func viewWillDisappear(_ animated: Bool)
    
}

protocol AuthUseCase: class {
    
    func requestLogin(email: String ,password: String)

}

protocol AuthInteractorOutput: class {
    func requestLoginSuccess()
    func requestLoginError(error: HttpResponseError)
    
}

protocol AuthWireFrame: class {
    func showHome()
}


