//
//  AuthViewController.swift
//  Iosys
//  Template by Weslley Madeira

//  Created by Weslley Madeira on 15/01/20.
//Copyright © 2020 Weslley Madeira. All rights reserved.


import UIKit


class AuthViewController: BaseViewController {
    
    var presenter: AuthPresenter!
    
    @IBOutlet weak var constraintButton: NSLayoutConstraint!
    @IBOutlet weak var constraintStackViewTop: NSLayoutConstraint!
    @IBOutlet weak var textFieldEmail: UITextField!
    @IBOutlet weak var textFieldPassword: UITextField!
    @IBOutlet weak var scrollView: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.enableKeyboardHideTap = true
        
        self.configKeyboard()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
       
        
    }
    
    //Actions
    @IBAction func enterApp(_ sender: UIButton) {
        self.endEditingMode()
        guard let email = textFieldEmail.text , let password = textFieldPassword.text  else { return }
        presenter.buttonLoginPressed(email: email, password: password)
    }
    
    func configKeyboard(){
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name:UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {

        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            UIView.animate(withDuration: 0.25, delay: 0, options: .curveEaseInOut, animations: {
                self.constraintButton.constant = keyboardHeight + 4
                self.constraintStackViewTop.constant = 16
                self.view.layoutIfNeeded()
            }) { (_) in
                
            }
        }
        
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
       UIView.animate(withDuration: 0.25, delay: 0, options: .curveEaseInOut, animations: {
        self.constraintButton.constant = 21.0
        self.constraintStackViewTop.constant = 63
            self.view.layoutIfNeeded()
        }) { (_) in
            
        }
     }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .darkContent
    }
}

extension AuthViewController: AuthView {
    
    
}
