//
//  AuthRouter.swift
//  IosysRouter
//  Template by Weslley Madeira

//  Created by Weslley Madeira on 15/01/20.
//  Copyright © 2020 Weslley Madeira. All rights reserved.


import UIKit


class AuthRouter {
    
    weak var view: BaseViewController?
   
    static func startView() -> AuthViewController {
        
        let viewController  = AuthViewController()
        let presenter       = AuthPresenter()
        let router          = AuthRouter()
        let interactor    = AuthInteractor()
        
        router.view = viewController
        
        viewController.presenter = presenter
        presenter.router = router
        presenter.view = viewController
        presenter.interactor = interactor
        
        interactor.output = presenter
        
        return viewController
    }
}

extension AuthRouter : AuthWireFrame {
    
    func showHome() {
        let home = HomeRouter.startView()
        
        self.view?.present(home, animated: true, completion: nil)
    }
}

