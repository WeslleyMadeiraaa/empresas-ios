//
//  RootContract.swift
//  Iosys
//  Template by Weslley Madeira

//  Created by Weslley Madeira on 15/01/20.
//Copyright © 2020 Weslley Madeira. All rights reserved.


import UIKit



protocol RootPresentation: class {
    func presentAuth()
    func presentHome()
}


protocol RootUseCase:class {
    func checkIsLogged()
}

protocol RootInterectorOutput: class {
    func presentAuth()
    func presentHome()
}

protocol RootWireFrame: class {
    func presentAuth()
    func presentHome()
}


