//
//  RootInteractor.swift
//  Iosys
//  Template by Weslley Madeira

//  Created by Weslley Madeira on 15/01/20.
//Copyright © 2020 Weslley Madeira. All rights reserved.


import UIKit

class RootInteractor: RootUseCase {
    
    var output: RootInterectorOutput?
    
    func checkIsLogged() {
        if SecurityAccess.shared?.isLogged ?? false {
            output?.presentHome()
        } else {
            output?.presentAuth()
        }
        
    }
}
