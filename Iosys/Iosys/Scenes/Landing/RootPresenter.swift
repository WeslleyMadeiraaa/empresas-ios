//
//  RootPresenter.swift
//  Iosys
//  Template by Weslley Madeira

//  Created by Weslley Madeira on 15/01/20.
//  Copyright © 2020 Weslley Madeira. All rights reserved.


import UIKit

import Foundation

class RootPresenter: RootPresentation {

    var router: RootWireFrame
    var interactor: RootInteractor
    
    init(router: RootRouter) {
        self.router = router
        self.interactor = RootInteractor()
        self.interactor.output = self
    }
    
    func presentInitialScreen() {
        interactor.checkIsLogged()
    }

}

extension RootPresenter: RootInterectorOutput {
    func presentAuth() {
        router.presentAuth()
    }
    
    func presentHome() {
        router.presentHome()
    }
    
   
}
