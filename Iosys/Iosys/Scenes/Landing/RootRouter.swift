//
//  RootRouter.swift
//  IosysRouter
//  Template by Weslley Madeira

//  Created by Weslley Madeira on 15/01/20.
//  Copyright © 2020 Weslley Madeira. All rights reserved.


import UIKit

class RootRouter: NSObject , RootWireFrame {
    
    
    var window : UIWindow
    
    init(window: UIWindow) {
        self.window = window
    }
    
    func startApp() {
        let presenter = RootPresenter(router: self)
        presenter.presentInitialScreen()
    }
    
    func presentAuth() {
        let root = AuthRouter.startView()
        window.rootViewController = root
        window.makeKeyAndVisible()
    }
    
    func presentHome() {
        let root = HomeRouter.startView()
        window.rootViewController = root
        window.makeKeyAndVisible()
    }
    
}


