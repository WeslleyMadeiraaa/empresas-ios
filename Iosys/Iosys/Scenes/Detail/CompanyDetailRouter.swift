//
//  CompanyDetailRouter.swift
//  IosysRouter
//  Template by Weslley Madeira

//  Created by Weslley Madeira on 15/01/20.
//  Copyright © 2020 Weslley Madeira. All rights reserved.


import UIKit


class CompanyDetailRouter {
    
    weak var view            : BaseViewController?

    static func startView(company: Company) -> CompanyDetailViewController {
        
        let viewController  = CompanyDetailViewController()
        let presenter       = CompanyDetailPresenter(company: company)
        let router          = CompanyDetailRouter()
        let interactor    = CompanyDetailInteractor()
        
        router.view = viewController
        
        viewController.presenter = presenter
        presenter.router = router
        presenter.view = viewController
        presenter.interactor = interactor
        
        interactor.output = presenter
        
        return viewController
    }
}

extension CompanyDetailRouter : CompanyDetailWireFrame {
    
   
}

