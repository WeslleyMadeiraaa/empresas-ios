//
//  CompanyDetailContract.swift
//  Iosys
//  Template by Weslley Madeira

//  Created by Weslley Madeira on 15/01/20.
//Copyright © 2020 Weslley Madeira. All rights reserved.


import UIKit


protocol CompanyDetailView: BaseView {
    
    func showImage(url: URL)
    func showDescription(description: String)
    func showTitle(title: String)
}

protocol CompanyDetailPresentation: class {
    var company: Company {get set}
    func viewDidLoad()
    func viewDidAppear()
    func viewWillAppear(_ animated: Bool)
    func viewDidDisappear(_ animated: Bool)
    func viewWillDisappear(_ animated: Bool)
    
}

protocol CompanyDetailUseCase: class {
    
}

protocol CompanyDetailInteractorOutput: class {
    
}

protocol CompanyDetailWireFrame: class {
    
}


