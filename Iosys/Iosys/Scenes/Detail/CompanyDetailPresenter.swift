//
//  CompanyDetailPresenter.swift
//  Iosys
//  Template by Weslley Madeira

//  Created by Weslley Madeira on 15/01/20.
//  Copyright © 2020 Weslley Madeira. All rights reserved.


import UIKit


class CompanyDetailPresenter {
    
    var company: Company
    
    weak var view: CompanyDetailView!
    var router: CompanyDetailWireFrame!
    var interactor: CompanyDetailUseCase!
    
    init(company: Company) {
        self.company = company
    }
    
    
}

extension CompanyDetailPresenter : CompanyDetailPresentation {
    
    func viewDidLoad(){
        
        self.view.showDescription(description: company.description)
        self.view.showTitle(title: company.enterpriseName)
        if let url = URL(string: company.photo ?? "") {
            self.view.showImage(url: url)
        }
    }
    
    func viewDidAppear(){
        
    }
    func viewWillAppear(_ animated: Bool){
        
    }
    func viewDidDisappear(_ animated: Bool){
        
    }
    func viewWillDisappear(_ animated: Bool){
        
    }
}

extension CompanyDetailPresenter: CompanyDetailInteractorOutput {
    
}
