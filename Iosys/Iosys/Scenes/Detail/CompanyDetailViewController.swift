//
//  CompanyDetailViewController.swift
//  Iosys
//  Template by Weslley Madeira

//  Created by Weslley Madeira on 15/01/20.
//Copyright © 2020 Weslley Madeira. All rights reserved.


import UIKit
import Kingfisher


class CompanyDetailViewController: BaseViewController {
    
    var presenter: CompanyDetailPresenter!
    
    @IBOutlet weak var imageLogo: UIImageView!
    @IBOutlet weak var labelDescripition: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewDidLoad()
        self.configNavigation()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func configNavigation() {
        navigationController?.configTitleTextWhite()
    }
}

extension CompanyDetailViewController: CompanyDetailView {
    
    func showTitle(title: String) {
        self.navigationItem.title = title
    }
    func showImage(url: URL) {
        self.imageLogo.kf.setImage(with: url)
    }
    
    func showDescription(description: String) {
        self.labelDescripition.text = description
    }
}
