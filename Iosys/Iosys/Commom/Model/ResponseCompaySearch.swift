//
//  ResponseCompaySearch.swift
//  Iosys
//
//  Created by Weslley Madeira on 15/01/20.
//  Copyright © 2020 Weslley Madeira. All rights reserved.
//

import Foundation


struct ResponseCompaySearch: Codable {
    
    var company: [Company]
    
    enum CodingKeys: String, CodingKey {
        case company = "enterprises"
    }
}
