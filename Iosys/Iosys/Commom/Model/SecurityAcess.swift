//
//  SecurityAcess.swift
//  Iosys
//
//  Created by Weslley Madeira on 15/01/20.
//  Copyright © 2020 Weslley Madeira. All rights reserved.
//

import Foundation

struct AccessData: Codable {
    
    var uid: String
    var client: String
    var accessToken: String
}


class SecurityAccess {
    
    private static var _instace : SecurityAccess?
    fileprivate var acessData: AccessData?
    
    static var shared: SecurityAccess? {
        if _instace == nil {
            let userDefaults = UserDefaults.standard
            if let deviceData = userDefaults.data(forKey: "AccessData") {
                do {
                    let acessData = try JSONDecoder().decode(AccessData.self, from: deviceData)
                    let securityacess = SecurityAccess()
                    securityacess.acessData = acessData
                    _instace = securityacess
                } catch  {
                    _instace = nil
                    
                    print(error)
                }
            } else {
                _instace = nil
            }
        }
        return _instace
        
    }
    
    static func saveAccessData(acessData: AccessData) {
        let userDefaults = UserDefaults.standard
        do {
            let userAppData = try JSONEncoder().encode(acessData.self)
            userDefaults.set(userAppData, forKey: "AccessData")
        } catch {
            print(error)
        }
    }
    
    var isLogged: Bool {
        if self.acessData != nil {
            return true
        } else {
            return false
        }
    }
    
    var getUid: String? {
        self.acessData?.uid
    }
    var getToken: String? {
        self.acessData?.accessToken
    }
    var getClient: String? {
        self.acessData?.client
    }
    
    
    
    
    
    
}
