//
//  BaseView.swift
//  Iosys
//
//  Created by Weslley Madeira on 15/01/20.
//  Copyright © 2020 Weslley Madeira. All rights reserved.
//

import Foundation

import UIKit

protocol BaseView: class {
    func showLoading(handle: (()->())?)
    func showProgressLoading(loadingMessage: String?)
    func updateProgressLoading(progress: Float)
    func hideLoading(handle: (()->())?)
    func showError(title: String)
    func showError(message: String)
    func showError(title: String, message: String)
    func showSuccess(title: String, message: String)
    func showAlertController(title: String?, message: String?)
    func openWebView(withUrl url: String)
}
