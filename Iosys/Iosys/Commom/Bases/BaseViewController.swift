//
//  BaseViewController.swift
//  Iosys
//
//  Created by Weslley Madeira on 15/01/20.
//  Copyright © 2020 Weslley Madeira. All rights reserved.
//
import UIKit

class BaseViewController: UIViewController {
    
    var tapGesture: UITapGestureRecognizer?
    
    var enableKeyboardHideTap: Bool = false {
        didSet {
            if enableKeyboardHideTap {
                self.view.addGestureRecognizer(tapGesture!)
            } else {
                self.view.removeGestureRecognizer(tapGesture!)
            }
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.endEditingMode))
    }
    
    @objc func endEditingMode(){
        self.view.endEditing(true)
    }
}

extension BaseViewController: BaseView {
    func showProgressLoading(loadingMessage: String?) {
        
    }
    
    func updateProgressLoading(progress: Float) {
        
    }
    
    func showError(title: String) {
        
    }
    
    func showError(message: String) {
        
    }
    
    func showError(title: String, message: String) {
        
    }
    
    func showSuccess(title: String, message: String) {
        
    }
    
    func showAlertController(title: String?, message: String?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let actionOK = UIAlertAction(title: "Ok", style: .default) { (action) in
            
        }
        alert.addAction(actionOK)
        self.present(alert, animated: true, completion: nil)
    }
    
    func openWebView(withUrl url: String) {
        
    }
    
    
}

extension BaseViewController {
    static var topViewController: UIViewController? {
        var top = UIApplication.shared.keyWindow?.rootViewController
        while let presentedViewController = top?.presentedViewController {
            top = presentedViewController
        }
        
        return top
    }
    static var currentViewController: UIViewController? {
        
        if  let navigationController = BaseViewController.topViewController as? UINavigationController {
            let top = navigationController.topViewController
            return top
            
        }
        return BaseViewController.topViewController
    }
    
    func showLoading(handle: (()->())?){
           
           DispatchQueue.main.async {
               self.creatLoading()
               handle?()
           }
           
           
           
       }
       func hideLoading(handle: (()->())?){
           
           DispatchQueue.main.async {
               if let viewController = BaseViewController.topViewController {
                   if let loadingView = viewController.view.viewWithTag(6699){
                       loadingView.removeFromSuperview()
                   }
               }
               handle?()
           }
           
       }
       
       func creatLoading(){
           if let viewControllerTop = BaseViewController.topViewController {
               if (viewControllerTop.view.viewWithTag(6699) != nil){
                   return
               }
               let viewLoading = UIView()
               viewLoading.translatesAutoresizingMaskIntoConstraints = false
               viewLoading.backgroundColor = UIColor.black.withAlphaComponent(0.5)
               viewLoading.tag = 6699
               
               let viewbackground = UIView()
               
               viewbackground.frame = CGRect(x: 0, y: 0, width: 80, height: 80)
               viewbackground.backgroundColor = .lightGray
               viewbackground.layer.cornerRadius = 8
               viewbackground.clipsToBounds = true
               viewbackground.translatesAutoresizingMaskIntoConstraints = false
               
               let activeIndicator = UIActivityIndicatorView()
               activeIndicator.style = .white
               activeIndicator.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
               activeIndicator.startAnimating()
               activeIndicator.translatesAutoresizingMaskIntoConstraints = false
               
               viewLoading.addSubview(viewbackground)
               viewbackground.addSubview(activeIndicator)
               viewControllerTop.view.addSubview(viewLoading)
               
               let constX = NSLayoutConstraint(item: activeIndicator, attribute: .centerX, relatedBy: .equal, toItem: viewbackground, attribute: .centerX, multiplier: 1.0, constant: 0)
               let constY = NSLayoutConstraint(item: activeIndicator, attribute: .centerY, relatedBy: .equal, toItem: viewbackground, attribute: .centerY, multiplier: 1.0, constant: 0)
               
               NSLayoutConstraint.activate([constX ,constY])
               
               
               viewControllerTop.view.addConstraintsWithFormat(format: "H:|[v0]|", views: viewLoading)
               viewControllerTop.view.addConstraintsWithFormat(format: "V:|[v0]|", views: viewLoading)
               
              
               
               NSLayoutConstraint.activate([viewbackground.centerYAnchor.constraint(equalTo: viewControllerTop.view.centerYAnchor),
                                            viewbackground.centerXAnchor.constraint(equalTo: viewControllerTop.view.centerXAnchor),
                                            viewbackground.heightAnchor.constraint(equalToConstant: 80),
                                            viewbackground.widthAnchor.constraint(equalToConstant: 80)])
               
               
           }
           
       }
}
