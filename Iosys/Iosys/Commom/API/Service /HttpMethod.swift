//
//  HttpMethod.swift
//  Iosys
//
//  Created by Weslley Madeira on 15/01/20.
//  Copyright © 2020 Weslley Madeira. All rights reserved.
//

import Foundation

enum HttpMethod {
    
    case get
    case post
    case put
    case delete
    
    var method: String {
        switch self {
        case .get    : return "GET"
        case .post   : return "POST"
        case .put    : return "PUT"
        case .delete : return "DELETE"
        }
    }
    
}
