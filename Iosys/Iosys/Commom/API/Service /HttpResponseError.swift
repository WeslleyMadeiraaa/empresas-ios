//
//  HttpResponseError.swift
//  Iosys
//
//  Created by Weslley Madeira on 15/01/20.
//  Copyright © 2020 Weslley Madeira. All rights reserved.
//

import Foundation


import Foundation


public class HttpResponseError : NSObject , Codable {
    
    var errorCode: NSInteger?
    var message: String?
    
    public init(code: NSInteger , messagem: String) {
        self.errorCode = code
        self.message = messagem
    }
    
    public init(code: NSInteger) {
        switch code {
        case 0...999:
            self.errorCode = code
            self.message = HTTPStatusCode.statusDescripition(code)
        default:
            self.errorCode = code
            self.message = HttpResponseError.getMenssageInternalError(code: code)
        }
    }
    
    static func getMenssageInternalError(code: NSInteger) -> String?{
        let localizableString = "INTERNAL_ERROR_" + String(code)
        return localizableString.localized
    }
}

public struct HTTPStatusCode{
    
    static func statusDescripition(_ code: NSInteger? ) -> String? {
        guard let code = code else {return nil}
        let description = "HTTP_CODE_" + String(code)
        return description.localized
    }
    
}
