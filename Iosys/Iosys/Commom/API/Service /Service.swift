//
//  Service.swift
//  Iosys
//
//  Created by Weslley Madeira on 15/01/20.
//  Copyright © 2020 Weslley Madeira. All rights reserved.
//

import Foundation
import Alamofire


class Service : NSObject {
    
    
    override init() {}
    private static var _instance : Service?
    private static var _instanceManager: SessionManager?
    private static var alamofireManager: SessionManager{
        if _instanceManager == nil {
            let configuration = URLSessionConfiguration.default
            configuration.timeoutIntervalForRequest = 30
            configuration.httpMaximumConnectionsPerHost = 3
            configuration.timeoutIntervalForResource = 60
            _instanceManager = Alamofire.SessionManager(configuration: configuration, delegate: .init(), serverTrustPolicyManager: nil)
        }
        return _instanceManager!
    }
    static var shared : Service {
        if _instance == nil {
            _instance = Service()
        }
        return _instance!
    }
    
    class func request<T: Codable>(request: IosysRequests, expered: T.Type  , completion: @escaping (_ responseData: HttpResponseSuccess<T>) -> () , fail: @escaping (HttpResponseError) -> ()) -> DataRequest {
        
        //MARK: - URL
        let url : URL = IosysConnections.makeURL(path: request.path)
        var urlRequest = URLRequest(url: url)
        
        // MARK: - POST
        if request.methodRequest == .post || request.methodRequest == .put {
            if ((request.body as? [String: Any]) != nil){
                urlRequest.httpBody = try? JSONSerialization.data(withJSONObject: request.body as Any, options: [])
            }else if let data = request.body as? Data {
                urlRequest.httpBody = data
                do {
                    let obj = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.allowFragments)
                    print(obj)
                }catch{
                    print(error)
                }
            }
            
            
        }else if request.methodRequest == .get{
            urlRequest.url = IosysConnections.makeURL(path: request.path, parameters: request.params!)
        }
        
        //MARK: - METHOD
        
        urlRequest.httpMethod = HTTPMethod.init(rawValue: request.methodRequest.method)!.rawValue
        
        // MARK: - HEADERS
        for (key , value) in request.headers {
            urlRequest.allHTTPHeaderFields?[key] = value
        }
        // MARK: - PRINTS
        print("\(request.methodRequest.method) || "  ,urlRequest.urlRequest ?? "Not URL" )
        
        //print("BODY: " ,)
        print("BODY: " , request.body ?? "")
        
        // MARK: - REQUEST
        
        let dataRequest = Service.alamofireManager.request(urlRequest).responseData { (data) in
            print("Response code: " , data.response?.statusCode ?? " No status")
            if data.error == nil {
                switch data.response?.statusCode {
                case 200, 201:
                    
                    let objSuccessfully = HttpResponseSuccess<T>(data: data.data , code: data.response?.statusCode , headers: data.response?.allHeaderFields)
                    completion(objSuccessfully)
                    //LOG: Data
                    print("Json: ----------------->\n " , objSuccessfully.jsonDictionary ?? "Error json")
                    //print("\n Object: -----------------> \n" , objSuccessfully.obj ?? "Error object")
                    break
                default:
                    fail(HttpResponseError(code: data.response?.statusCode ?? 0))
                    break
                }
            }else{
                print("Error:\n" , data.error ?? "Error nil")
                fail(HttpResponseError(code: 0, messagem: data.error?.localizedDescription ?? "Error"))
               
            }
        }
    
        return dataRequest
    }
    
    
}
