//
//  HttpResponse.swift
//  Iosys
//
//  Created by Weslley Madeira on 15/01/20.
//  Copyright © 2020 Weslley Madeira. All rights reserved.
//

import Foundation


class HttpResponseSuccess<Element: Codable> : NSObject {
    
    var data: Data?
    var code: Int?
    var headers: [AnyHashable: Any]?
    
    var successs: Bool? {
        return HttpResponseCode.success(code)
    }
    var statusDescripition: String? {
        return HttpResponseCode.statusDescripition(code)
    }
    var errorObjct: HttpResponseError?
    var obj: Element?
    var jsonDictionary: NSDictionary?{
        do {
            let objData = try JSONSerialization.jsonObject(with: self.data!, options: JSONSerialization.ReadingOptions.allowFragments)
            return objData as? NSDictionary
        }catch{
            errorObjct = HttpResponseError(code: 1001)
            print(error)
            return nil
        }
    }
    
    public init(data: Data? , code: NSInteger? , headers: [AnyHashable: Any]? ){
        super.init()
        self.headers = headers
        self.data = data
        self.code = code
        self.mountElement()
    }
    private func mountElement(){
        if let data = data {
            do {
                self.obj = try JSONDecoder().decode(Element.self, from: data)
            }catch{
                errorObjct = HttpResponseError(code: 1001)
                print(error)
                self.obj = nil
            }
        }else{
            self.obj = nil
        }
    }
}



struct HttpResponseCode {
    
    // MARK: - StatusCode
    
    static func success(_ code: Int?) -> Bool? {
        // MARK:: - RAGE OF SUCCESS
        guard let code = code else {return nil}
        switch code {
        case 200...206:
            return true
        default:
            return false
        }
        
        
    }
    static func statusDescripition(_ code: Int? ) -> String? {
        guard let code = code else {return nil}
        switch code  {
        case 200...206:
            //MARK: - SUCCESSFULL DESCRIPTION 2xxx
            switch code {
            case 200:
                return "Success"
            case 201:
                return "Created"
            case 202:
                return "Accepted"
            case 203:
                return "NonAuth"
            case 204:
                return "NoContent"
            case 205:
                return "RstContent"
            case 206:
                return "PrtContent"
            default:
                return nil
            }
            
        case 400...406:
            //MARK: CLIENT ERROR - 4xxx
            switch code {
            case 400:
                return "Bad Request"
            case 401:
                return "Un Authorized"
            case 402:
                return "Forbidden"
            case 403:
                return "Not Found"
            case 404:
                return "Not Allowed"
            case 405:
                return "Not Acceptable"
            case 406:
                return "Not Acceptable"
            default:
                return nil
            }
            
        case 500...504:
            // MARK: - SERVER ERROR - 5xx
            switch code {
                
            case 500:
                return "Internal Error"
            case 501:
                return "Not Implemented"
            case 502:
                return "Bad Gateway"
            case 503:
                return "Service Unavailable"
            case 504:
                return "Gateway Timeout"
            default:
                return nil
            }
        default:
            break
        }
        return nil
    }
    //        // MARK: - DomainError - 100x
    //        public struct DomainError {
    //            public static let Offline : (String, Int, String) = ("DomainError", -1009, "Sem conexão com a internet")
    //            public static let FacebookLogin : (String, Int, String) = ("DomainError", -2000, "Ocorreu um erro ao realizar o login com o Facebook, por favor, tente novamente")
    //            public static let Register : (String, Int, String) = ("DomainError", -2001, "Ocorreu um erro ao realizar a operação, por favor, tente novamente")
    //            public static let CepNotFound : (String, Int, String) = ("DomainError", -2002, "Nenhum endereço encontrado no CEP informado")
    //            public static let Unknown : (String, Int, String) = ("DomainError", 404, "Ocorreu um erro na requisição, por favor, tente novamente")
    //
    //        }
    
    
}
