//
//  IosysRequests.swift
//  Iosys
//
//  Created by Weslley Madeira on 15/01/20.
//  Copyright © 2020 Weslley Madeira. All rights reserved.
//

import Foundation


enum IosysRequests{
    
    case singIN(email: String , password: String)
    case getSearchCompany(textSearch: String)
   
    
    var params: [String : Any]? {
        switch self {
        case .getSearchCompany(let text):
            return ["enterprise_types" : 1,
                    "name"             : text ]
        default:
            return [:]
        }
    }
    var path: String {
        switch self {
        case .singIN:
            return "/users/auth/sign_in"
        case .getSearchCompany:
            return "/enterprises"
        }
    }
    
    var methodRequest: HttpMethod {
        switch self {
        case .singIN:
            return .post
        case .getSearchCompany:
            return .get
        }
    }
    
    var body: Any? {
        switch self {
        case .singIN(let email,let password):
            return [ "email": email,
                     "password": password]
        default:
            return nil
        }
        
    }
    
    var headers : [String: String] {
        switch self {
            
        default:
            let paramsBase = ["culture"       : "pt-br",
                              "Content-Type"  : "application/json",
                              "Accept"        : "application/json",
                              "uid"           :  SecurityAccess.shared?.getUid ?? "",
                              "client"        :  SecurityAccess.shared?.getClient ?? "",
                              "access-token"  :  SecurityAccess.shared?.getToken ?? ""]
            return paramsBase
        }
        
        
    }
    
    
}
