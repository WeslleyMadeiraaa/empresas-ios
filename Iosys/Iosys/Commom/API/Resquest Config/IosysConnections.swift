//
//  IosysConnections.swift
//  Iosys
//
//  Created by Weslley Madeira on 15/01/20.
//  Copyright © 2020 Weslley Madeira. All rights reserved.
//

import Foundation


class IosysConnections: NSObject {
    
    struct components {
        static var scheme: String{
            return "https"
        }
        
        static var versionApi: String {
            return "v1"
        }
        
        static var host: String {
            return "empresas.ioasys.com.br"
        }
        
        static var port: Int? {
            return nil
        }
        
    }
    
    static func makeURL(path: String, parameters: Dictionary<String, Any> = [:]) -> URL{
        
        var urlComponents = URLComponents()
        urlComponents.scheme = IosysConnections.components.scheme
        urlComponents.host = IosysConnections.components.host
        if let port = IosysConnections.components.port {
            urlComponents.port = port
        }
        urlComponents.path = "/api/\(IosysConnections.components.versionApi)\(path)"
        
        for (key, value) in parameters {
            if urlComponents.queryItems == nil {
                urlComponents.queryItems = [URLQueryItem]()
            }
            urlComponents.queryItems?.append(URLQueryItem(name: key, value: "\(value)"))
        }
        return urlComponents.url!
    }
    
}
