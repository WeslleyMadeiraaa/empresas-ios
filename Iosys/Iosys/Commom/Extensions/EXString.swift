//
//  EXString.swift
//  Iosys
//
//  Created by Weslley Madeira on 15/01/20.
//  Copyright © 2020 Weslley Madeira. All rights reserved.
//

import Foundation

extension String {
    var localized: String {
          return NSLocalizedString(self, comment: "")
    }

}
