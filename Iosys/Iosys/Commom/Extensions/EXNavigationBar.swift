//
//  EXNavigationBar.swift
//  Iosys
//
//  Created by Weslley Madeira on 15/01/20.
//  Copyright © 2020 Weslley Madeira. All rights reserved.
//

import Foundation
import UIKit


extension UINavigationController {

    func clearStyle() {
        navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationBar.shadowImage = UIImage()
        navigationBar.isTranslucent = true
        navigationBar.tintColor = UIColor(named: "BackgroundNavigation")
        view.backgroundColor = .clear
    }
    
    func applyIosysStyle() {
        navigationBar.barTintColor = UIColor(named: "BackgroundNavigation")
        navigationBar.isTranslucent = false
        navigationBar.setBackgroundImage(UIColor(named: "BackgroundNavigation")!.asOnePointImage(), for: .default)
        navigationBar.tintColor = UIColor(named: "BackgroundNavigation")
        navigationBar.isTranslucent = false
        navigationBar.backIndicatorImage = UIImage(named: "icBack")
        navigationBar.backIndicatorTransitionMaskImage = UIImage(named: "icBack")
        navigationBar.tintColor = UIColor(named: "White")
        let backButton = UIBarButtonItem()
        backButton.title = ""
        navigationBar.topItem?.backBarButtonItem = backButton
    }
    
    func configTitleTextWhite() {
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.whiteColor]
        navigationBar.titleTextAttributes = textAttributes
    }
    
    open override var childForStatusBarStyle: UIViewController? {
           return visibleViewController
    }
    
    
}

extension UINavigationItem {
    
    func insertLogoLogin() {
        let image = UIImage.logoNavi?.withRenderingMode(.alwaysTemplate)
        
        let imageView = UIImageView(image: image)
        imageView.tintColor = UIColor.white
        
        titleView = imageView
    }
    
    func insertIosysLogo() {
        titleView = UIImageView(image: UIImage.logoNavi)
        
    }
    
}
