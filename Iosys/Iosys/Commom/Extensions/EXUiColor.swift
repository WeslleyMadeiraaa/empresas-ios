//
//  EXUiColor.swift
//  Iosys
//
//  Created by Weslley Madeira on 15/01/20.
//  Copyright © 2020 Weslley Madeira. All rights reserved.
//

import Foundation
import UIKit


extension UIColor {
    
    public class var backgroundColor: UIColor { return UIColor(named: "BackgroundColor")! }
    public class var backgroundNavigation: UIColor { return UIColor(named: "BackgroundNavigation")! }
    public class var backgrounfButtonColor: UIColor { return UIColor(named: "BackgrounfButtonColor")! }
    public class var fontBlackColor: UIColor { return UIColor(named: "FontBlackColor")! }
    public class var fontGreyColor: UIColor { return UIColor(named: "FontGreyColor")! }
    public class var greenImagens: UIColor { return UIColor(named: "GreenImagens")! }
    public class var whiteColor: UIColor { return UIColor(named: "White")! }
    
    func asOnePointImage() -> UIImage? {
           UIGraphicsBeginImageContext(CGSize(width: 1, height: 1))
           let context = UIGraphicsGetCurrentContext()
           setFill()
           context?.fill(CGRect(x: 0, y: 0, width: 1, height: 1))
           let image = UIGraphicsGetImageFromCurrentImageContext()
           UIGraphicsEndImageContext()
           return image
    }
    func imageWithColor(width: Int, height: Int) -> UIImage {
        let size = CGSize(width: width, height: height)
        return UIGraphicsImageRenderer(size: size).image { rendererContext in
            self.setFill()
            rendererContext.fill(CGRect(origin: .zero, size: size))
        }
    }
}
