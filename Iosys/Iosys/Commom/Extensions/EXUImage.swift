//
//  EXUIColor.swift
//  Iosys
//
//  Created by Weslley Madeira on 15/01/20.
//  Copyright © 2020 Weslley Madeira. All rights reserved.
//

import Foundation
import UIKit

extension UIImage {
    
    public class var logoNavi: UIImage? { return UIImage(named: "logoNav") }
    public class var icSearch: UIImage? { return UIImage(named: "icSearch") }
    
}
