//
//  EXView.swift
//  Iosys
//
//  Created by Weslley Madeira on 15/01/20.
//  Copyright © 2020 Weslley Madeira. All rights reserved.
//

import Foundation
import UIKit


extension UIView {
    func addConstraintsWithFormat(format: String, views: UIView...) {
         
         var viewDictionary = [String : UIView]()
         
         for (index, view) in views.enumerated() {
             let key = "v\(index)"
             view.translatesAutoresizingMaskIntoConstraints = false
             viewDictionary[key] = view
         }
         
         self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: format, options: NSLayoutConstraint.FormatOptions(), metrics: nil, views: viewDictionary))
         
     }
}
